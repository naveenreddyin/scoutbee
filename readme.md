# Scoutbee Chat application
## Prerequisites 
1. Docker:
* This project cant be run without Docker and a configuration made to it. Even for tests to run, you would need services run by Docker compose.
* This also will help to make this project scalable, as services such as Elasticsearch is also used to have proper search and which is a defacto when it comes to searching backend now.
* Also Docker is used because it provides consistency in installation on a new system.
* To download and install Docker, [click here](https://docs.docker.com/docker-for-mac/install/) or relevant flavor based on your OS.
* There is a configuration needed when Docker is installed to help Elasticsearch service run. It needs to have 4gb of memory, default set is 2gb. This [link](https://www.petefreitag.com/item/848.cfm) will help you with that. Note: This is very essential that all services are run in green for tests or the service in general to run.

2. The next step would be to clone the repository, and than going inside that repository using command line.
3. Once Docker is properly installed and configured (i.e 4gb), now it is time to run the docker-compose:
* `docker-compose up --build`
* It might take some time for the services to run, it has to be made sure that all of the services are run good.
4. If all the services are up, than you could go to frontend react app through [http://localhost](http://localhost), and backend is at port 8000 which could be accessed using this [link](http://localhost:8000) and Elasticsearch through this [link](http://localhost:9200).
5. There are tests added as well,
* For backend test and coverage you first need to install requirements in some virtualenv or conda virtualenv, and than by going inside backend folder run, `pip install -r requirements.txt` and once everything is installed, to test and see coverage run `python -m pytest -s -v` being in the root folder.
* For frontend test and coverage you first need to install dependencies, so go inside the folder frontend and than run `npm install` and than run `npm test -- --coverage` from frontend root folder.
## Main points and Tech stack:
1. This project uses nginx as proxy server.
2. Reactjs on frontend with redux and redux-saga and axios wrapper for fetching data. You should get a modal if the user is not in localstorage and than if you enter and user is unique and not in db a new chat user is created on backend and modal disappears and than you can send the chat. You could open 2 or 3 browsers to see the real time chat. The search bar on top fetches the results through backend rest api which in turn gets it from Elasticsearch. If results are there than it is presented using a black box, not that beauty of an UI but works for now :)
3. Websockets are being used as well using Django channels which is setup on backend and the backend also serves rest end point. The backend websocket server doesnt do any blocking call i.e model or db calling etc. as it will not be productive. 
4. Elasticsearch for searching is used and it is indexed when we build the app on Docker and than every time a new chat message is saved than it gets synced directly in Elasticsearch indices. 
5. Postgres database is used for storing messages and chat users. 
6. Redis is used for Django channels i.e websockets.