import apisauce from 'apisauce'

import { API_URL } from '../config'

const create = (baseURL = `${API_URL}`) => {

    const api = apisauce.create({
        baseURL,
        headers: {
        },
        timeout: 10000
    })

    const checkUser = (user) => api.post('user', { 'name': user })
    const storeMessage = (user, message) => api.post('message', { 'user': user, 'message': message })
    const fetchMessages = () => api.get('message')
    const search = (query) => api.get(`search/?message=${query}`)

    return {
        checkUser,
        storeMessage,
        fetchMessages,
        search,
    }
}

export default {
    create
}