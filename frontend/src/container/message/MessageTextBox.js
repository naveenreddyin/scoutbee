import { connect } from 'react-redux';

import Component from '../../components/message/MessageTextBox';
import ChatActions from '../../redux/ChatRedux';

const mapStateToProps = (state) => {
    return {
        message: state.chat.message
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        sendMessage: _ => dispatch(ChatActions.sendMessage()),
        setMessage: (message) => dispatch(ChatActions.setMessage(message))
    }
}
const MessageTextBoxContainer = connect(mapStateToProps, mapDispatchToProps)(Component)

export default MessageTextBoxContainer;