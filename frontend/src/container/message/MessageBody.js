import { connect } from 'react-redux';

import Component from '../../components/message/MessageBody';

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages,
        results: state.search.results,
    }
}

const MessageBodyContainer = connect(mapStateToProps)(Component)

export default MessageBodyContainer;