import { put, call, select } from 'redux-saga/effects';
import SearchActions from '../redux/SearchRedux';

const getSearch = state => state.search;

export function* sendQuerySaga(api) {
    let { query } = yield select(getSearch);
    let response = null;
    let results = null;

    if (query) {
        response = yield call(api.search, query)
        console.log(response)
        if (response.status === 200) {
            if (response.data.length > 0) {
                results = response.data.map((item) => ({ user: item.user.name, message: item.message }))
                yield put(SearchActions.setResults(results))
            }
        }
    }
}

