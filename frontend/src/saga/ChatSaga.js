import { put, call, select } from 'redux-saga/effects';
import ChatActions from '../redux/ChatRedux';

const getChat = state => state.chat;
const getUser = state => state.user;
const getSocket = state => state.socket;

export function* sendMessageSaga(api) {
    let { message } = yield select(getChat);
    let { user } = yield select(getUser);
    let { socket } = yield select(getSocket);

    if (message) {
        socket.send(JSON.stringify({
            'message': message,
            'user': user
        }));
        // also store messages using rest api
        const response = yield call(api.storeMessage, user, message);
        if(response.status === 201)
            yield put(ChatActions.setMessage(null))
    }

}

export function* fetchMessagesSaga(api) {
    const response = yield call(api.fetchMessages);
    if (response.status === 200) {
        yield put(ChatActions.setMessagesFromServer(response.data))
    }
}
