import { put, call } from 'redux-saga/effects';
import UserActions from '../redux/UserRedux';

export function* checkUserSaga(api, { user }) {

    const response = yield call(api.checkUser, user);

    if (response.status === 201)
        yield put(UserActions.setUser(user, null, true));
    else
        yield put(UserActions.setUser(null, response.data.name[0], false));
}
