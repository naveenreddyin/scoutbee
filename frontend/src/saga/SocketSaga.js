import { eventChannel } from 'redux-saga';
import { call, put, take } from 'redux-saga/effects';
import { WEBSOCKET_URL } from '../config';
import { SocketTypes } from '../redux/SocketRedux';
import { ChatTypes } from '../redux/ChatRedux';


function initWebsocket() {
    return eventChannel(emitter => {
        const socket = new WebSocket(WEBSOCKET_URL)
        socket.onopen = () => {
            console.log('connected')
            emitter({ type: SocketTypes.SET_SOCKET, socket })
        }
        socket.onerror = (error) => {
            console.log('WebSocket error ' + error)
            console.dir(error)
        }
        socket.onmessage = (e) => {
            let message = null
            try {
                message = JSON.parse(e.data)
            } catch (e) {
                console.error(`Error parsing : ${e.data}`)
            }
            if (message) {
                return emitter({ type: ChatTypes.SET_MESSAGES_FROM_SERVER, messages: message })
            }
        }
        // unsubscribe function
        return () => {
            console.log('Socket off')
        }
    })
}

export function* wsSagas() {
    const channel = yield call(initWebsocket)
    while (true) {
        const action = yield take(channel)
        yield put(action)
    }
}