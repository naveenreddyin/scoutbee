import { takeEvery } from 'redux-saga/effects';

import { UserTypes } from '../redux/UserRedux';
import { ChatTypes } from '../redux/ChatRedux';
import { SocketTypes } from '../redux/SocketRedux';
import { SearchTypes } from '../redux/SearchRedux';
import { checkUserSaga } from './UserSaga';
import { wsSagas } from './SocketSaga';
import { sendMessageSaga, fetchMessagesSaga } from './ChatSaga';
import { sendQuerySaga } from './SearchSaga';

import API from '../services/Api';

const api = API.create()

export default function* root() {
    yield* [
        takeEvery(UserTypes.CHECK_USER, checkUserSaga, api),
        takeEvery(SocketTypes.START_SOCKET, wsSagas),
        takeEvery(ChatTypes.SEND_MESSAGE, sendMessageSaga, api),
        takeEvery(ChatTypes.FETCH_MESSAGES, fetchMessagesSaga, api),
        takeEvery(SearchTypes.SEND_QUERY, sendQuerySaga, api)
    ]
}