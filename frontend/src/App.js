import React, { Component } from 'react';

import { Provider } from 'react-redux'

import createStore from './redux'
import Main from './components/Main'

const store = createStore()

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

export default App;
