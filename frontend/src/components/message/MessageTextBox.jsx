import React from 'react';

import { HeroFooter, Field, Control, Input, Column, Columns, Button } from 'bloomer';

const MessageTextBox = (props) => {

    return (
        <HeroFooter style={{ marginBottom: 50 }}>
            <Field>
                <Control>
                    <Columns>
                        <Column>
                            <Input type="text" placeholder='Start chatting...'
                                value={props.message || ''}
                                onChange={(event) => props.setMessage(event.target.value)}
                                onKeyPress={(event) => {
                                    if (event.charCode === 13 || event.keyCode === 13) {
                                        props.sendMessage();
                                        event.target.value = '';
                                    }
                                }}
                            />
                        </Column>
                        <Column>
                            <Button isColor={'info'} onClick={props.sendMessage}>
                                Send
                            </Button>
                        </Column>
                    </Columns>
                </Control>
            </Field>
        </HeroFooter>
    );
};

export default MessageTextBox;