import React from 'react';

import { HeroBody, Container, Title, Hero } from 'bloomer';

import Messages from './Messages';

const MessageBody = (props) => {

    let { messages } = props;
    let { results } = props;

    return (
        <HeroBody>
            <Container>
                {results.length > 0 &&
                    <Hero isColor='dark' isSize={'large'}>
                        <Title tag='h1'>Search Results</Title>
                        <Messages data={results} />
                    </Hero>
                }
                {messages.length > 0 &&
                    <Messages data={messages} />
                }
            </Container>
        </HeroBody>
    );
};

export default MessageBody;