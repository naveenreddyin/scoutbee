import React from 'react';

import { Columns, Column } from 'bloomer';

const appStorage = window.localStorage;
const user = appStorage.getItem('name');

const Messages = (props) => {

    let { data } = props;

    return (
        <>
            {data.map((result, i) => (
                <Columns key={i}>
                    <Column hasTextAlign={'left'}>
                        <p>{result.user === user ? 'You' : result.user}:</p>
                    </Column>
                    <Column hasTextAlign={'left'}>
                        <p>{result.message}</p>
                    </Column>
                </Columns>
            ))
            }
        </>
    );
}

export default Messages;