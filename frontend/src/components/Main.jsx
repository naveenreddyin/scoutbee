import React, { Component } from 'react';

import { connect } from 'react-redux'
import { Container, Hero } from 'bloomer';

import MessageBody from '../container/message/MessageBody';
import MessageTextBox from '../container/message/MessageTextBox';
import UserModal from '../components/UserModal';
import UserActions from '../redux/UserRedux';
import SocketActions from '../redux/SocketRedux';
import ChatActions from '../redux/ChatRedux';
import SearchActions from '../redux/SearchRedux';
import SearchBox from '../components/SearchBox';

export class Main extends Component {

    componentDidMount() {
        this.props.startSocket();
        this.props.fetchMessages();
    }

    render() {

        return (
            <Container isFluid>
                <UserModal {...this.props} />
                <Hero isColor='info' isFullHeight={true}>
                    <SearchBox {...this.props}/>
                    <MessageBody />
                    <MessageTextBox />
                </Hero>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        checkAndSetUser: user => {
            dispatch(UserActions.checkUser(user))
        },
        handleNameChange: user => {
            dispatch(UserActions.handleNameChange(user))
        },
        startSocket: _ => {
            dispatch(SocketActions.startSocket())
        },
        fetchMessages: _ => {
            dispatch(ChatActions.fetchMessages())
        },
        search: _ => {
            dispatch(SearchActions.sendQuery())
        },
        handleQueryChange: query => {
            dispatch(SearchActions.setQuery(query))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main)
