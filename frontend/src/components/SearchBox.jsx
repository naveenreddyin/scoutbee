import React from 'react';

import PropTypes from 'prop-types';

import { Columns, Column, Field, Control, Input, Button } from 'bloomer';

const SearchBox = (props) => {

    return (
        <Columns>
            <Column>
                <Field>
                    <Control>
                        <Input type="text" placeholder='Search...' onChange={(event) => props.handleQueryChange(event.target.value)} />
                    </Control>
                </Field>
            </Column>
            <Column>
                <Button isColor={'info'} onClick={props.search}>
                    Search
                </Button>
            </Column>
        </Columns>
    )
}

SearchBox.propTypes = {
    search: PropTypes.func.isRequired,
    handleQueryChange: PropTypes.func.isRequired,
};

export default SearchBox;