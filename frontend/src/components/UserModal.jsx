import React from 'react';

import { Modal, ModalBackground, ModalContent, Field, Control, Input, Box, Button, Column, Columns } from 'bloomer';

const UserModal = (props) => {

    return (
        <Modal isActive={props.user.checked ? false : true}>
            <ModalBackground />
            <ModalContent>
                <Box>
                    <Field>
                        <Control>
                            <Columns>
                                <Column>
                                    <Input type="text" placeholder='Type your username.' onChange={e => props.handleNameChange(e.target.value)} />
                                </Column>
                                <Column>
                                    <Button
                                        isColor={'warning'}
                                        onClick={() => {
                                            if (props.user.user)
                                                props.checkAndSetUser(props.user.user);
                                        }}
                                    >
                                        Submit
                                    </Button>
                                </Column>
                            </Columns>
                            <p>{props.user.error}</p>
                        </Control>
                    </Field>
                </Box>
            </ModalContent>
        </Modal>
    );
}

export default UserModal;