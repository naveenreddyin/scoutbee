import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    sendMessage: [],
    setMessage: ['message'],
    setMessagesFromServer: ['messages'],
    fetchMessages: []
});

export const ChatTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    message: null,
    messages: []
});

/* ------------- Reducers ------------- */

export const sendMessage = (state: Object, object: Object) => {
    return { ...state };
}

export const setMessage = (state: Object, { message }: Object) => {
    return { ...state, message };
}

export const setMessagesFromServer = (state: Object, { messages }: Object) => {
    return { ...state, messages: state.messages.concat(messages) };
}

export const fetchMessages = (state: Object, object: Object) => {
    return { ...state };
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
    [Types.SEND_MESSAGE]: sendMessage,
    [Types.SET_MESSAGE]: setMessage,
    [Types.SET_MESSAGES_FROM_SERVER]: setMessagesFromServer,
    [Types.FETCH_MESSAGES]: fetchMessages
});