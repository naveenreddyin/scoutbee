import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    startSocket: [],
    stopSocket: [],
    setSocket: ['socket'],
});

export const SocketTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    socket: null,
});

/* ------------- Reducers ------------- */

export const startSocket = (state: Object, object: Object) => {
    return { ...state }
}

export const stopSocket = (state: Object, object: Object) => {
    return { ...state }
}

export const setSocket = (state: Object, { socket }: Object) => {
    return { ...state, socket }
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
    [Types.STOP_SOCKET]: stopSocket,
    [Types.START_SOCKET]: startSocket,
    [Types.SET_SOCKET]: setSocket
});