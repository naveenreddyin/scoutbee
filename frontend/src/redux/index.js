import { combineReducers } from 'redux'
import configureStore from './CreateStore'

import rootSaga from '../saga'

export default () => {
    /* ------------- Assemble The Reducers ------------- */
    const rootReducer = combineReducers({
        user: require('./UserRedux').reducer,
        chat: require('./ChatRedux').reducer,
        socket: require('./SocketRedux').reducer,
        search: require('./SearchRedux').reducer,
    })

    return configureStore(rootReducer, rootSaga)
}