import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    setQuery: ['query'],
    setResults: ['results'],
    sendQuery: [],
});

export const SearchTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    results: [],
    query: null
});

/* ------------- Reducers ------------- */

export const sendQuery = (state: Object, object: Object) => {
    return { ...state };
}

export const setQuery = (state: Object, { query }: Object) => {
    return { ...state, query };
}

export const setResults = (state: Object, { results }: Object) => {
    return { ...state, results };
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
    [Types.SEND_QUERY]: sendQuery,
    [Types.SET_QUERY]: setQuery,
    [Types.SET_RESULTS]: setResults,
});