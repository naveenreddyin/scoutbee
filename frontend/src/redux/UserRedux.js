import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const appStorage = window.localStorage;

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    checkUser: ['user'],
    setUser: ['user', 'error', 'checked'],
    handleNameChange: ['user']
});

export const UserTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    user: appStorage.getItem('name') || null,
    error: null,
    checked: appStorage.getItem('name') ? true : false,
});

/* ------------- Reducers ------------- */

export const checkUser = (state: Object, object: Object) => {
    return state.merge({})
}

export const setUser = (state: Object, { user, error, checked }: Object) => {
    appStorage.setItem('name', user);
    return state.merge({ user, error, checked })
}

export const handleNameChange = (state: Object, { user }: Object) => {
    return state.merge({ user: user })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
    [Types.CHECK_USER]: checkUser,
    [Types.SET_USER]: setUser,
    [Types.HANDLE_NAME_CHANGE]: handleNameChange,
});