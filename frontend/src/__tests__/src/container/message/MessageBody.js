import React from 'react';

import { configure, mount } from 'enzyme';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store'

import MessageBody from '../../../../container/message/MessageBody';

configure({ adapter: new Adapter() });


describe('MessageBody container testing', () => {

    let wrapper, store;
    let mockStore = configureStore();
    let initialState = {
        chat: { messages: [] }, search: { results: [] }
    };

    beforeEach(() => {
        store = mockStore(initialState);

        wrapper = mount(
            <Provider store={store}>
                <MessageBody />
            </Provider>
        );
    });

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

});
