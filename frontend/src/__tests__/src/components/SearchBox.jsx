import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Columns, Column, Field, Control, Input, Button } from 'bloomer';

import SearchBox from '../../../components/SearchBox';

configure({ adapter: new Adapter() });


describe('SearchBox component testing', () => {

    let props, wrapper;

    beforeEach(() => {
        props = {
            search: jest.fn(),
            handleQueryChange: jest.fn()
        };
        wrapper = shallow(<SearchBox {...props} />);
    });

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('contains 1 Columns element', () => {
        expect(wrapper.find(Columns)).toHaveLength(1);
    });

    it('should contain 2 Column elements', () => {
        expect(wrapper.find(Column)).toHaveLength(2);
    });

    it('should contain 2 Field elements', () => {
        expect(wrapper.find(Field)).toHaveLength(1);
    });

    it('should contain 2 Control elements', () => {
        expect(wrapper.find(Control)).toHaveLength(1);
    });

    it('should contain 1 Input element', () => {
        expect(wrapper.find(Input)).toHaveLength(1);
    });

    it('should contain 1 Button element', () => {
        expect(wrapper.find(Button)).toHaveLength(1);
    });

    it('should call search props function when clicked on Button', () => {
        wrapper.find(Button).simulate('click');
        expect(props.search).toHaveBeenCalled();
    });
});