import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Modal, ModalBackground, ModalContent, Field, Control, Input, Box, Button, Column, Columns } from 'bloomer';

import UserModal from '../../../components/UserModal';

configure({ adapter: new Adapter() });


describe('UserModal component testing', () => {

    let props, wrapper;

    beforeEach(() => {
        props = { user: { checked: false, user: 'Naveen' }, checkAndSetUser: jest.fn() }
        wrapper = shallow(<UserModal {...props} />)
    });

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('has a defined structure', () => {
        expect(wrapper.find(Modal)).toHaveLength(1);
        expect(wrapper.find(ModalBackground)).toHaveLength(1);
        expect(wrapper.find(ModalContent)).toHaveLength(1);
        expect(wrapper.find(Box)).toHaveLength(1);
        expect(wrapper.find(Field)).toHaveLength(1);
        expect(wrapper.find(Control)).toHaveLength(1);
        expect(wrapper.find(Input)).toHaveLength(1);
        expect(wrapper.find(Button)).toHaveLength(1);
        expect(wrapper.find(Columns)).toHaveLength(1);
        expect(wrapper.find(Column)).toHaveLength(2);
    });

    it('responds to prop changes', () => {
       wrapper.find(Button).simulate('click');
       expect(props.checkAndSetUser).toHaveBeenCalled();
    });
});