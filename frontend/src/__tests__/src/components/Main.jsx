import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Main from '../../../components/Main';

configure({ adapter: new Adapter() });


describe('Main component testing', () => {

    let props, wrapper;

    beforeEach(() => {
        props = { search: jest.fn() };
        wrapper = shallow(<Main {...props} />);
    });

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });
});