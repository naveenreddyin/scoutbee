import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { HeroBody, Container, Hero, Title } from 'bloomer';

import MessageBody from '../../../../components/message/MessageBody';
import Messages from '../../../../components/message/Messages';

configure({ adapter: new Adapter() });


describe('MessageBody component testing', () => {

    let props, wrapper;

    beforeEach(() => {
        props = { messages: [], results: [] }
        wrapper = shallow(<MessageBody {...props} />)
    });

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('should have proper structure', () => {
        expect(wrapper.find(HeroBody)).toHaveLength(1);
        expect(wrapper.find(Container)).toHaveLength(1);
    });

    it('should have Messages component when props change', () => {
        expect(wrapper.find(Messages)).toHaveLength(0);
        wrapper.setProps({ messages: [{ user: 'Naveen', messsage: 'Hello World' }] });
        expect(wrapper.find(Messages)).toHaveLength(1);
        // if results are added it should show two Messages component
        wrapper.setProps({
            results: [{ user: 'Naveen', messsage: 'Hello World' }],
            messages: [{ user: 'Naveen', messsage: 'Hello World' }]
        });
        expect(wrapper.find(Messages)).toHaveLength(2);
        expect(wrapper.find(Hero)).toHaveLength(1);
        expect(wrapper.find(Title)).toHaveLength(1);
    });
});