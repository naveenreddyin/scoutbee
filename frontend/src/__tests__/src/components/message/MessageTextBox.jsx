import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Columns, Column, Field, Control, HeroFooter, Input, Button } from 'bloomer';

import MessageTextBox from '../../../../components/message/MessageTextBox';

configure({ adapter: new Adapter() });


describe('MessageTextBox component testing', () => {

    let props, wrapper;

    beforeEach(() => {
        props = {
            sendMessage: jest.fn(),
            setMessage: jest.fn()
        }
        wrapper = shallow(<MessageTextBox {...props} />)
    });

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('has a proper struucture', () => {
        expect(wrapper.find(HeroFooter)).toHaveLength(1);
        expect(wrapper.find(Field)).toHaveLength(1);
        expect(wrapper.find(Control)).toHaveLength(1);
        expect(wrapper.find(Columns)).toHaveLength(1);
        expect(wrapper.find(Column)).toHaveLength(2);
        expect(wrapper.find(Input)).toHaveLength(1);
        expect(wrapper.find(Button)).toHaveLength(1);
    });

    it('triggers props functions on events', () => {
        wrapper.find(Button).simulate('click');
        expect(props.sendMessage).toHaveBeenCalled();
        // Input onChange
        wrapper.find(Input).simulate('change', { target: { value: 'Hello' } });
        expect(props.setMessage).toHaveBeenCalledTimes(1);
        // on keypress
        wrapper.find(Input).simulate('keyress', {
            key: 'Enter',
            keyCode: 13,
            which: 13,
        });
        expect(props.sendMessage).toHaveBeenCalled();
    });
});