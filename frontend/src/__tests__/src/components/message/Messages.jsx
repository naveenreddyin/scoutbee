import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Columns, Column } from 'bloomer';

import Messages from '../../../../components/message/Messages';

configure({ adapter: new Adapter() });


describe('Messages component testing', () => {

    let props, wrapper;

    beforeEach(() => {
        props = {data: []}
        wrapper = shallow(<Messages {...props} />)
    });

    it('renders without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('has one Columns and two Column and two P element when props are passed', () => {
        expect(wrapper.find(Columns)).toHaveLength(0);
        expect(wrapper.find(Column)).toHaveLength(0);
        expect(wrapper.find('p')).toHaveLength(0);
        // set the props
        wrapper.setProps({data: [{user: 'Naveen', message: 'Hello World'}]})
        expect(wrapper.find(Columns)).toHaveLength(1);
        expect(wrapper.find(Column)).toHaveLength(2);
        expect(wrapper.find('p')).toHaveLength(2);
        expect(wrapper.find('p').at(0).text()).toEqual('Naveen:');
        expect(wrapper.find('p').at(1).text()).toEqual('Hello World');
    });
});