import React from 'react';

import { configure, mount } from 'enzyme';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';

import App from '../../App';
import Main from '../../components/Main';

configure({ adapter: new Adapter() });

describe('App testing', () => {

  let wrapper, store, mockStore, initialState;
  initialState = {};
  mockStore = configureStore();

  beforeEach(() => {
    store = mockStore(initialState);
    wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>
    );
  });

  it('renders without crashing', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders one Main container', () => {
    expect(wrapper.find(Main)).toHaveLength(1);
  });

});