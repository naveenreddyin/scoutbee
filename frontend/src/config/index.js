export const USE_LOGGING = process.env.NODE_ENV === "development" || false

export const API_URL = window.location.protocol + '//' + window.location.hostname + ':8000';

export const WEBSOCKET_URL = 'ws://'+window.location.hostname+':8000/ws/chat/';

