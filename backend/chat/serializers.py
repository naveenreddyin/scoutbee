import json

from rest_framework import serializers
from django_elasticsearch_dsl_drf.serializers import DocumentSerializer

from .models import ChatMessage, ChatUser
from .documents import ChatMessageDocument


class ChatUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatUser
        fields = '__all__'


class ChatMessageSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.name')

    class Meta:
        model = ChatMessage
        fields = ('user', 'message')

    def create(self, validated_data):
        user = validated_data.pop('user')
        user_instance = ChatUser.objects.get(name=user.get('name'))
        message_instance = ChatMessage.objects.create(**validated_data, user=user_instance)
        return message_instance


class ChatMessageDocumentSerializer(DocumentSerializer):
    class Meta(object):
        document = ChatMessageDocument
        fields = (
            'user',
            'message',
        )
