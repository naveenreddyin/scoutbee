from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .views import CreateChatUserAPIView, ChatMessageAPIView, ChatMessageDocumentView

router = DefaultRouter()
router.register(r'search',
                ChatMessageDocumentView,
                base_name='chatmessagedocument')

urlpatterns = [
    path('user', CreateChatUserAPIView.as_view()),
    path('message', ChatMessageAPIView.as_view()),
    path('', include(router.urls)),
]
