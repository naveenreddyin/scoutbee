from django.db import models


class ChatUser(models.Model):
    name = models.CharField(max_length=100, unique=True)


class ChatMessage(models.Model):
    user = models.ForeignKey(ChatUser, on_delete=models.CASCADE)
    message = models.TextField()