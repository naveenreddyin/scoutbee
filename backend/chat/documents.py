from django_elasticsearch_dsl import DocType, Index, fields
from django_elasticsearch_dsl_drf.compat import KeywordField, StringField
from django_elasticsearch_dsl_drf.analyzers import edge_ngram_completion

from .models import ChatMessage, ChatUser

# Name of the Elasticsearch index
messages = Index('messages')

messages.settings(
    number_of_shards=1,
    number_of_replicas=0
)


@messages.doc_type
class ChatMessageDocument(DocType):

    user = fields.ObjectField(properties={
        'name': fields.TextField(),
    })

    message = StringField(
        fields={
            'raw': KeywordField(),
            'suggest': fields.CompletionField(),
            'edge_ngram_completion': fields.StringField(
                analyzer=edge_ngram_completion
            ),
        }
    )

    class Meta:
        model = ChatMessage  # The model associated with this DocType

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
        ]
        related_models = [ChatUser, ]

        def get_queryset(self):
            return super(ChatMessageDocument, self).get_queryset().select_related(
                'user'
            )

    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, ChatUser):
            return related_instance.chatmessage_set.all()
