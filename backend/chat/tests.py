import pytest
import json

from django.db.utils import IntegrityError
from django.core.management import call_command
from django.urls import reverse

from channels.testing import WebsocketCommunicator
from concurrent.futures._base import TimeoutError
from rest_framework.test import APIClient
from rest_framework import status

from .consumers import ChatConsumer
from .models import ChatUser, ChatMessage


@pytest.mark.asyncio
async def test_chat_consumer():
    communicator = WebsocketCommunicator(ChatConsumer, "/ws/chat/")
    connected, subprotocol = await communicator.connect()
    assert connected
    data = json.dumps({"message": "hello", "user": "Scoutbee"})
    await communicator.send_to(text_data=data)
    response = await communicator.receive_from()
    response_data = json.loads(response)
    assert response_data.get("message") == "hello"
    await communicator.disconnect()


@pytest.mark.django_db
def test_with_chat_user_model():
    call_command('search_index', '--rebuild', '-f')
    ChatUser.objects.create(name="Scoutbee")
    assert ChatUser.objects.count() == 1
    # Test for unique ness
    with pytest.raises(IntegrityError):
        ChatUser.objects.create(name="Scoutbee")


@pytest.mark.django_db
def test_with_chat_message_model():
    user1 = ChatUser.objects.create(name="Scoutbee")
    user2 = ChatUser.objects.create(name="SAP")
    ChatMessage.objects.create(user=user1, message="Hello SAP")
    ChatMessage.objects.create(user=user2, message="Hello Scoutbee")


@pytest.fixture
def apiclient():
    return APIClient()


@pytest.mark.django_db
def test_chat_user_create_view(apiclient):
    # should not cater GET request
    response = apiclient.get('/user', format='json')
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    response = apiclient.post('/user', format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {"name": ["This field is required."]}
    # Send proper request now to expect 201
    response = apiclient.post('/user', {
                              'name': 'Scoutbee'
                              }, format='json')
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data == {'id': 1, 'name': 'Scoutbee'}
    # Also check in db it should have a count of 1 after success
    assert ChatUser.objects.count() == 1
    # Try to create one more user with same name and expect an error
    response = apiclient.post('/user', {
                              'name': 'Scoutbee'
                              }, format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {"name": ["chat user with this name already exists."]}


@pytest.mark.django_db
def test_chat_message_list_create_view(apiclient):
    # should cater GET request too
    response = apiclient.get('/message', format='json')
    assert response.status_code == status.HTTP_200_OK
    # send wrong request and should be an error ;)
    response = apiclient.post('/message', format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    # even when supplied just with one of the required fields
    # it should be still 400
    response = apiclient.post('/message',
                              {
                                  'message': 'First message'
                              },
                              format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    response = apiclient.post('/message',
                              {
                                  'user': {'name': 'Scoutbee'}
                              },
                              format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    # create proper response now and it should be 201, for that also create a user
    ChatUser.objects.create(name='Naveen')
    response = apiclient.post('/message',
                              {
                                  'user': 'Naveen',
                                  'message': 'First message'
                              },
                              format='json')
    assert response.status_code == status.HTTP_201_CREATED
    # Also check in database if count is 1
    assert ChatMessage.objects.count() == 1


@pytest.mark.django_db
def test_search_view(apiclient):
    data = {}
    url = reverse('chatmessagedocument-list', kwargs={})
    response = apiclient.get(url, data)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 2
    url = '{}?message={}'.format(url, "first")
    response = apiclient.get(url, data)
    print(url)
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 1
