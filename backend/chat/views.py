from rest_framework import generics

from django_elasticsearch_dsl_drf.viewsets import BaseDocumentViewSet
from django_elasticsearch_dsl_drf.filter_backends import (
    FilteringFilterBackend,
    IdsFilterBackend,
    OrderingFilterBackend,
    DefaultOrderingFilterBackend,
    SearchFilterBackend,
)
from django_elasticsearch_dsl_drf.constants import (
    LOOKUP_FILTER_TERMS,
    LOOKUP_FILTER_RANGE,
    LOOKUP_FILTER_PREFIX,
    LOOKUP_FILTER_WILDCARD,
    LOOKUP_QUERY_IN,
    LOOKUP_QUERY_GT,
    LOOKUP_QUERY_GTE,
    LOOKUP_QUERY_LT,
    LOOKUP_QUERY_LTE,
    LOOKUP_QUERY_EXCLUDE,
)

from .serializers import ChatMessageSerializer, ChatUserSerializer, ChatMessageDocumentSerializer
from .models import ChatMessage, ChatUser
from .documents import ChatMessageDocument


class CreateChatUserAPIView(generics.CreateAPIView):
    queryset = ChatUser.objects.all()
    serializer_class = ChatUserSerializer


class ChatMessageAPIView(generics.ListCreateAPIView):
    queryset = ChatMessage.objects.all()
    serializer_class = ChatMessageSerializer


class ChatMessageDocumentView(BaseDocumentViewSet):
    document = ChatMessageDocument
    serializer_class = ChatMessageDocumentSerializer
    lookup_field = 'message'
    search_fields = (
        'user',
        'message',
    )
    filter_backends = [
        FilteringFilterBackend,
        IdsFilterBackend,
        OrderingFilterBackend,
        DefaultOrderingFilterBackend,
        SearchFilterBackend,
    ]
    filter_fields = {
        'user': 'user',
        'message': 'message', 
    }
    # Define ordering fields
    ordering_fields = {
        'message': 'message',
    }
    # Specify default ordering
    ordering = ('message',)
